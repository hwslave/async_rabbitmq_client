#!/usr/bin/env python
#coding:utf-8
"""
  Author:   --<v1ll4n>
  Purpose: Publisher
  Created: 09/18/17
"""

import pika

try:
    import queue
except:
    import Queue as queue

import time
import logging

from .common import BaseClient

class RabbitPublisher(BaseClient):
    """"""

    def initial(self):
        """"""
        self.logger = logging.getLogger('rabbitmq.publisher')
        self._working = True
        self.default_exchange = None
        self.default_publish_interval = 0.1
        self.default_routing_key = 0.1
        self.publish_queue = queue.Queue()
    
    @property
    def exchanges(self):
        """"""
        return [i['exchange'] for i in self._exchange_list]
    
    def set_default_exchange(self, exchange_name):
        """"""
        assert exchange_name in self.exchanges, 'you should add this exchange first.'
        
        self.default_exchange = exchange_name
    
    def set_default_routing_key(self, routing_key):
        """"""
        self.default_routing_key = routing_key
        
    def set_publish_interval(self, publish_interval=0.1):
        """"""
        self.default_publish_interval = publish_interval
        
    def confirm_delivery(self):
        """"""
        self.channel.confirm_delivery(self.on_confirm_delivery)
    
    def on_confirm_delivery(self, method_frame):
        """"""
        self.logger.debug('recv a confirm information.')
    
    def publish_message(self):
        """"""
        # from publish_queue getting message
        body, exchange, routingkey = self.publish_queue.get()
        
        # default
        exchange = exchange if exchange else self.default_exchange
        routingkey = routingkey if routingkey else self.default_routing_key
        
        self.logger.debug('publish a message:{} from publish queue to {}:{}'.\
                          format(body, exchange, routingkey))
        
        try: 
            self.channel.basic_publish(
                exchange,
                routingkey,
                body,
                )
            
        except:
            self.logger.debug('publish failed.')
            self.publish_queue.put((body, exchange, routingkey))
        
        return time.time() + self.default_publish_interval
            
    def main(self):
        """"""
        self.logger.debug('enter the main loop')
        next_pub = 0
        while self._working:
            if not self.publish_queue.empty():
                now = time.time()
                if now >= next_pub:
                    next_pub = self.publish_message()
        
        self.logger.debug('exit the main loop')


class BlockingRabbitPublisher(object):
    """"""

    #----------------------------------------------------------------------
    def __init__(self, host='127.0.0.1', 
                 port=5672, vhost='/',
                 username='guest', 
                 password='guest',
                 queue_name='test',
                 queue_delare_config={},
                 **config):
        """Constructor"""
        # user defined
        self._host = host
        self._port = port
        self._vhost = vhost
        self._username = username
        self._password = password
        self.rabbit_queue_name = queue_name
        assert isinstance(queue_delare_config, dict), 'queue config must be a dict'
        self.rabbit_queue_config = queue_delare_config

        # extra connection config
        self._config = config

        self._closed = False

        # setting
        self._cred = pika.PlainCredentials(self._username,
                                           self._password)
        self._param = pika.ConnectionParameters(
            host=self._host,
            port=self._port,
            credentials=self._cred,
            virtual_host=self._vhost,
            **self._config
        )

        # logger
        self.logger = logging.getLogger('rabbitmq')

        self._exchange_list = []
        self._exchange_map_rkey = {}

        # buffer
        self._exchange_buffer_queue = queue.Queue()
        self._bind_buffer_queue = queue.Queue()

        self.initial()

    def initial(self):
        """"""
        self.logger = logging.getLogger('rabbitmq.publisher')
        self._working = True
        self.default_exchange = None
        self.default_publish_interval = 0.1
        self.default_routing_key = 0.1
        self.publish_queue = queue.Queue() 

    @property
    def exchanges(self):
        """"""
        return [i[0] for i in self._exchange_list]

    def set_default_exchange(self, exchange_name):
        """"""
        assert exchange_name in self.exchanges, 'you should add this exchange first.'

        self.default_exchange = exchange_name

    def set_default_routing_key(self, routing_key):
        """"""
        self.default_routing_key = routing_key

    def set_publish_interval(self, publish_interval=0.1):
        """"""
        self.default_publish_interval = publish_interval    

    def add_exchange(self, exchange, exchange_type, **config):
        """"""
        # add a exchange
        exchange = (exchange, exchange_type, config)
        self._exchange_list.append(exchange)


    def connect(self):
        """"""
        self.logger.debug('connecting to rabbitmq.')
        self.connection = pika.BlockingConnection(self._param)
        self.logger.debug('connected.')

    def reset_channel(self):
        """"""
        self.logger.debug('creating the channel.')
        self.channel = self.connection.channel()
        self.logger.debug('created.')

        self.logger.debug('delare exchanges.')
        for (exchange, etype, config) in self._exchange_list:
            self.channel.exchange_declare(exchange,
                                          etype,
                                          **config)
        self.logger.debug('success.')


    def publish_message(self):
        """"""
        _ret = self.publish_queue.get()
        body, exchange, routingkey = _ret

            # default
        exchange = exchange if exchange else self.default_exchange
        routingkey = routingkey if routingkey else self.default_routing_key

        self.logger.debug('publish a message:{} from publish queue to {}:{}'.\
                          format(body, exchange, routingkey))

        try:    
            self.channel.basic_publish(
                exchange,
                routingkey,
                body,
            )
        except pika.exceptions.ChannelClosed:
            self.reset_channel()
            self.publish_queue.put(_ret)
        except pika.exceptions.ConnectionClosed:
            self.connect()
            self.reset_channel()
            self.publish_queue.put(_ret)

        return time.time() + self.default_publish_interval

    def mainloop(self):
        """"""
        # connection
        self.connect()
        self.reset_channel() 

        self.logger.debug('enter the main loop')
        next_pub = 0
        while self._working:
            if not self.publish_queue.empty():
                now = time.time()
                if now >= next_pub:
                    next_pub = self.publish_message()

    def feed(self, body, exchange=None, routing_key=None):
        """"""
        exchange = exchange if exchange else self.default_exchange
        routing_key = routing_key if routing_key else self.default_routing_key

        assert isinstance(body, str)
        assert isinstance(exchange, str)
        assert isinstance(routing_key, str)

        _ret = (body, exchange, routing_key)
        self.publish_queue.put(_ret)

    def run(self):
        """"""
        failed = False
        try:
            self.mainloop()
        except pika.exceptions.ConnectionClosed:
            failed = True

        if failed and not self._closed:
            self.run()

    def stop(self):
        """"""
        self._closed = True
        self.connection.close()